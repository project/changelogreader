<?php

/**
 * @file
 * This file holds the functions for the changelogreader Admin settings.
 */

/**
 * Menu callback; displays the changelogreader module settings page.
 *
 * @ingroup forms
 */
function changelogreader_admin_settings($form_state) {
  // Get themed table
  $table = _changelogreader_get_entries(FALSE, TRUE);
  // Put together form
  $form['changelogreader_op'] = array(
    '#type' => 'value',
    '#value' => arg(3),
  );
  $form['changelogreader_last_updated'] = array(
    '#type' => 'item',
    '#title' => t('Changelog Reader Cron Last Run'),
    '#value' => variable_get('changelogreader_last_updated', NULL) ? format_date(variable_get('changelogreader_last_updated', NULL), 'long') : t('never'),
    '#description' => l(t('Run Cron'), 'admin/reports/status/run-cron'),
  );
  $form['changelogreader_table'] = array(
    '#value' => $table,
  );

  // Return form
  return $form;
}

/**
 * Menu callback; Add form
 *
 * @ingroup forms
 */
function changelogreader_admin_add($form_state) {
  // Create form
  $form['changelogreader_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL of Site'),
    '#description' => t('Enter the base URL of the site where the CHANGELOG.tx would be.  Include trailing slash.  For example: %example', array('%example' => 'http://example.org/')),
    '#maxlength' => 512,
    '#required' => TRUE,
  );
  $form['changelogreader_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of Site'),
    '#description' => t('Enter a descriptive title for the site.'),
    '#maxlength' => 512,
    '#required' => TRUE,
  );
  $form['changelogreader_add'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  $form['changelogreader_cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );
  
  // Return form
  return $form;
}

/**
 * Add Form submit handler.
 *
 * @ingroup forms
 */
function changelogreader_admin_add_submit($form, &$form_state) {
  if ($form_state['values']['op'] == t('Cancel')) {
    // Redirect
    drupal_goto('admin/settings/changelogreader/overview');
  }
  // Make entry in DB
  db_query("INSERT INTO {changelogreader} (url, name) VALUES ('%s', '%s')", $form_state['values']['changelogreader_url'], $form_state['values']['changelogreader_name']);
  // Set message
  drupal_set_message(t('Item added.'));
  // Redirect
  drupal_goto('admin/settings/changelogreader/overview');
}

/**
 * Menu callback; Edit form
 *
 * @ingroup forms
 */
function changelogreader_admin_edit($form_state) {
  // Load entry
  $item = _changelogreader_get_entry(arg(4));
  // Check item found
  if (!is_array($item)) {
    drupal_goto('admin/settings/changelogreader/overview');
  }

  // Create form
  $form['changelogreader_id'] = array(
    '#type' => 'value',
    '#value' => arg(4),
  );
  $form['changelogreader_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL of Site'),
    '#description' => t('Enter the base URL of the site where the CHANGELOG.tx would be.  Include trailing slash.  For example: %example', array('%example' => 'http://example.org/')),
    '#maxlength' => 512,
    '#required' => TRUE,
    '#default_value' => $item['url'],
  );
  $form['changelogreader_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of Site'),
    '#description' => t('Enter a descriptive title for the site.'),
    '#maxlength' => 512,
    '#required' => TRUE,
    '#default_value' => $item['name'],
  );
  $form['changelogreader_add'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );
  $form['changelogreader_cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );
  
  // Return form
  return $form;
}

/**
 * Edit Form submit handler.
 *
 * @ingroup forms
 */
function changelogreader_admin_edit_submit($form, &$form_state) {
  if ($form_state['values']['op'] == t('Cancel')) {
    // Redirect
    drupal_goto('admin/settings/changelogreader/overview');
  }
  // Make entry in DB
  db_query("UPDATE {changelogreader} SET url = '%s', name = '%s' WHERE cid = %d", $form_state['values']['changelogreader_url'], $form_state['values']['changelogreader_name'], $form_state['values']['changelogreader_id']);
  // Set message
  drupal_set_message(t('Item updated.'));
  // Redirect
  drupal_goto('admin/settings/changelogreader/overview');
}

/**
 * Menu callback; Delete form
 *
 * @ingroup forms
 */
function changelogreader_admin_delete($form_state) {
  // Load entry
  $item = _changelogreader_get_entry(arg(4));
  // Check item found
  if (!is_array($item)) {
    drupal_goto('admin/settings/changelogreader/overview');
  }
  
  // Question
  $question = t('Are you sure you want to delete: %name', array('%name' => $item['name']));

  // Create form
  $form['changelogreader_id'] = array(
    '#type' => 'value',
    '#value' => arg(4),
  );
  
  // Return form
  return confirm_form($form, $question, 'admin/settings/changelogreader/overview', t('This action cannot be undone.'), t('Delete'), t('Cancel'), 'changelogreader_delete_confirm');
}

/**
 * Delete Form submit handler.
 *
 * @ingroup forms
 */
function changelogreader_admin_delete_submit($form, &$form_state) {
  // Make entry in DB
  db_query("DELETE FROM {changelogreader} WHERE cid = %d", $form_state['values']['changelogreader_id']);
  // Set message
  drupal_set_message(t('Item deleted.'));
  // Redirect
  drupal_goto('admin/settings/changelogreader/overview');
}

/**
 * Get ChangeLog entries
 *
 * @param $id
 *   Entry id
 * @return
 *   Array of values or false
 */
function _changelogreader_get_entry($id) {
  // Create query
  $query = "SELECT * FROM {changelogreader} WHERE cid = %d";
  return db_fetch_array(db_query($query, $id)); 
}