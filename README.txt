Changelog Reader:
-----------------------------
This module reads the CHANGELOG.txt from different sites.  It was created for a blog post I wrote pointing out how many of the Drupal shops and big Drupal sites are not up to date (and inherently not secure).  My point was not to say that the people running these sites are bad people, but that upgrading and maintaining Drupal sites is not something that is as seamless as it could be.  It's all in the spirit of open source.

Anyway, this module, allows you to collect site urls, then during cron, it goes to those sites, tries to find a CHANGELOG.txt and reads the version if found.  Then it provides a page and a block to display this information.

Please note that the CHANGELOG.txt is not there on every site as some people remove it.  Nor is it any guarantee that that version is being run on that site.  It's just a good guess.


Install:
-----------------------------
1) Basic Drupal module install.
2) Permissions: Administer ChangelogReader
3) Go to: "admin/settings/changelogreader"
   and start adding sites
4) Wait for or fun cron manually.
5) Enable the block, or go to "changelogreader"


To do:
-----------------------------
As this is a pretty pointless module, and made for a very specific purpose, I will probably not add too many features.  But if you are interested in adding features, please feel free to contact me to be a co-maintainer.


Author:
-----------------------------
Alan Palazzolo (zzolo)